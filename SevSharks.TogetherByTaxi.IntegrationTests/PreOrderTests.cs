﻿using System;
using System.Threading.Tasks;
using SevSharks.TogetherByTaxi.IntegrationTests.Models;
using Xunit;

namespace SevSharks.TogetherByTaxi.IntegrationTests
{
    public class PreOrderTests : BaseIntegrationTests
    {
        private readonly string _preOrderBaseUrl;

        private static LocationWithAddress LocationWithAddressFrom1 => new LocationWithAddress
        {
            Address = "ул. Ленина, 22, Севастополь",
            Lat = 44.6111715,
            Lng = 33.5255887999999
        };

        private static LocationWithAddress LocationWithAddressTo1 => new LocationWithAddress
        {
            Address = "ул. Гоголя, 22, Севастополь",
            Lat = 44.5940778,
            Lng = 33.5184244
        };

        private static LocationWithAddress LocationWithAddressFrom2 => new LocationWithAddress
        {
            Address = "ул. Суворова, 39, Севастополь",
            Lat = 44.6043181,
            Lng = 33.5238137
        };

        private static LocationWithAddress LocationWithAddressTo2 => new LocationWithAddress
        {
            Address = "просп. Генерала Острякова, 260, Севастополь",
            Lat = 44.5525643,
            Lng = 33.5294716
        };

        public PreOrderTests()
        {
            _preOrderBaseUrl = Flurl.Url.Combine(GetPreOrderApiUrl(), "api", "v1", "preorder");
        }

        [Fact]
        public async Task PreOrder_Unauthorized_Cycle()
        {
            var currentDateTime = DateTime.UtcNow;
            var preOrderUniqId = await CreatePreOrder();

            var getPreOrderByUniqIdResult = await GetPreOrder(preOrderUniqId, currentDateTime, LocationWithAddressFrom1, LocationWithAddressTo1);
            Assert.False(getPreOrderByUniqIdResult.Modified.HasValue);

            await ChangePreOrder(preOrderUniqId);

            getPreOrderByUniqIdResult = await GetPreOrder(preOrderUniqId, currentDateTime, LocationWithAddressFrom2, LocationWithAddressTo2);
            Assert.True(getPreOrderByUniqIdResult.Modified.HasValue);
        }

        private async Task<Guid> CreatePreOrder()
        {
            var model = new PreOrderCreateModel
            {
                LocationFromAddress = LocationWithAddressFrom1.Address,
                LocationFromLat = LocationWithAddressFrom1.Lat,
                LocationFromLng = LocationWithAddressFrom1.Lng,
                LocationToAddress = LocationWithAddressTo1.Address,
                LocationToLat = LocationWithAddressTo1.Lat,
                LocationToLng = LocationWithAddressTo1.Lng
            };
            var preOrderId = await PostAsync<Guid, PreOrderCreateModel>(_preOrderBaseUrl, model);
            Assert.NotEqual(Guid.Empty, preOrderId);
            return preOrderId;
        }

        private async Task<GetPreOrderByUniqIdResult> GetPreOrder(Guid preOrderUniqId, DateTime currentDateTime, LocationWithAddress locationFrom, LocationWithAddress locationTo)
        {
            var getPreOrderUrl = Flurl.Url.Combine(_preOrderBaseUrl, preOrderUniqId.ToString());
            var getPreOrderByUniqIdResult = await GetAsync<GetPreOrderByUniqIdResult>(getPreOrderUrl);
            Assert.NotNull(getPreOrderByUniqIdResult);
            Assert.Null(getPreOrderByUniqIdResult.CreatedUserId);
            Assert.False(getPreOrderByUniqIdResult.IsAuthorized);
            Assert.False(getPreOrderByUniqIdResult.IsDeleted);
            Assert.False(getPreOrderByUniqIdResult.IsOrderCreated);
            Assert.True(getPreOrderByUniqIdResult.Created >= currentDateTime);
            Assert.Equal(preOrderUniqId, getPreOrderByUniqIdResult.UniqIdentifier);

            CheckLocation(getPreOrderByUniqIdResult.LocationFrom, locationFrom);
            CheckLocation(getPreOrderByUniqIdResult.LocationTo, locationTo);
            return getPreOrderByUniqIdResult;
        }

        private async Task ChangePreOrder(Guid preOrderUniqId)
        {
            var changePreOrderUrl = Flurl.Url.Combine(_preOrderBaseUrl, preOrderUniqId.ToString());

            var model = new PreOrderChangeModel
            {
                LocationFromAddress = LocationWithAddressFrom2.Address,
                LocationFromLat = LocationWithAddressFrom2.Lat,
                LocationFromLng = LocationWithAddressFrom2.Lng,
                LocationToAddress = LocationWithAddressTo2.Address,
                LocationToLat = LocationWithAddressTo2.Lat,
                LocationToLng = LocationWithAddressTo2.Lng
            };
            var preOrderId = await PutAsync<PreOrderChangeModel, Guid>(changePreOrderUrl, model);
            Assert.Equal(preOrderUniqId, preOrderId);
        }

        private void CheckLocation(LocationResult locationResult, LocationWithAddress locationWithAddress)
        {
            Assert.NotNull(locationResult);
            Assert.NotNull(locationWithAddress);
            Assert.Equal(locationWithAddress.Address, locationResult.Address);
            Assert.Equal(locationWithAddress.Lat, locationResult.Lat);
            Assert.Equal(locationWithAddress.Lng, locationResult.Lng);
        }
    }
}
