using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Xunit;

namespace SevSharks.TogetherByTaxi.IntegrationTests
{
    public abstract class BaseIntegrationTests
    {
        protected readonly IConfigurationRoot Configuration;

        protected BaseIntegrationTests()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile("secrets/appsettings.secrets.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

        protected void StartTestLogMessage(string testName)
        {
            LogMessage("-------------------------------------------------------------");
            LogMessage($"START TEST {testName}");
        }

        protected void EndTestLogMessage(string testName)
        {
            LogMessage($"END TEST {testName}");
            LogMessage("-------------------------------------------------------------");
        }

        #region PreOrder Api settings

        protected string GetPreOrderApiUrl()
        {
            var result = Configuration["PreOrderApiUrl"];
            return result;
        }

        #endregion PreOrder Api settings

        #region Auth settings

        protected string GetAuthUrl()
        {
            var result = Configuration["Auth:Url"];
            return result;
        }

        protected string GetClientId()
        {
            var result = Configuration["Auth:ClientId"];
            return result;
        }

        protected string GetClientSecret()
        {
            var result = Configuration["Auth:ClientSecret"];
            return result;
        }

        #endregion Auth settings

        #region Common methods

        protected void LogMessage(string message)
        {
            var formattedMessage = $"{DateTime.UtcNow:O}: {message}";
            Trace.WriteLine(formattedMessage);
            Console.WriteLine(formattedMessage);
        }

        protected async Task WaitForTime(DateTime? time)
        {
            if (!time.HasValue)
            {
                return;
            }

            while (DateTime.Now < time)
            {
                LogMessage($"{time - DateTime.Now} to wait...");
                await Task.Delay(TimeSpan.FromSeconds(5));
            }
        }

        #endregion Common methods

        #region http client implementation

        public async Task<T> GetAsync<T>(string url, string authToken = null)
        {
            using var client = GetClient();
            if (!string.IsNullOrEmpty(authToken))
            {
                //Add the authorization header
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
            }

            var result = await client.GetAsync(url);
            var json = await result.Content.ReadAsStringAsync();
            if (!result.IsSuccessStatusCode)
            {
                LogMessage($"Get ERROR: {json}");
            }
            Assert.True(result.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public async Task<HttpResponseMessage> GetAsync(string url, string authToken = null)
        {
            using var client = GetClient();
            if (!string.IsNullOrEmpty(authToken))
            {
                //Add the authorization header
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
            }

            return await client.GetAsync(url);
        }

        /// <summary>
        /// POST ������ � �������
        /// </summary>
        /// <typeparam name="T">�����</typeparam>
        /// <typeparam name="TModelToSend">���� �������</typeparam>
        /// <param name="urlToSend">�����</param>
        /// <param name="modelToSend">������</param>
        /// <param name="authToken">�����</param>
        /// <returns></returns>
        protected async Task<T> PostAsync<T, TModelToSend>(string urlToSend, TModelToSend modelToSend, string authToken = null)
        {
            using var client = GetClient();
            if (!string.IsNullOrEmpty(authToken))
            {
                //Add the authorization header
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
            }

            var myContent = JsonConvert.SerializeObject(modelToSend);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            LogMessage($"Trying to send request to {urlToSend}");
            var result = await client.PostAsync(urlToSend, byteContent);
            var json = await result.Content.ReadAsStringAsync();
            if (!result.IsSuccessStatusCode)
            {
                LogMessage($"POST ERROR: {json}");
            }
            Assert.True(result.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            return JsonConvert.DeserializeObject<T>(json);
        }

        protected async Task<HttpResponseMessage> PostAsync<TModelToSend>(string urlToSend, TModelToSend modelToSend,
            string authToken = null)
        {
            using var client = GetClient();
            if (!string.IsNullOrEmpty(authToken))
            {
                //Add the authorization header
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
            }

            var byteContent = GetByteArrayContentForRequest(modelToSend);
            var result = await client.PostAsync(urlToSend, byteContent);
            return result;
        }

        protected async Task<HttpResponseMessage> PutAsync<TModelToSend>(string urlToSend, TModelToSend modelToSend,
            string authToken = null)
        {
            using var client = GetClient();
            if (!string.IsNullOrEmpty(authToken))
            {
                //Add the authorization header
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
            }
            var byteContent = GetByteArrayContentForRequest(modelToSend);
            var result = await client.PutAsync(urlToSend, byteContent);
            return result;
        }

        protected async Task<TResult> PutAsync<TModelToSend, TResult>(string urlToSend, TModelToSend modelToSend,
            string authToken = null)
        {
            using var client = GetClient();
            if (!string.IsNullOrEmpty(authToken))
            {
                //Add the authorization header
                client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse("Bearer " + authToken);
            }
            var myContent = JsonConvert.SerializeObject(modelToSend);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            LogMessage($"Trying to send request to {urlToSend}");
            var result = await client.PutAsync(urlToSend, byteContent);
            var json = await result.Content.ReadAsStringAsync();
            if (!result.IsSuccessStatusCode)
            {
                LogMessage($"PUT ERROR: {json}");
            }
            Assert.True(result.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            return JsonConvert.DeserializeObject<TResult>(json);
        }

        protected HttpClient GetClient()
        {
            //if (IgnoreIncorrectCertificate)
            //{
            //    var handler = new WebRequestHandler { ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true };
            //    return new HttpClient(handler);
            //}
            return new HttpClient();
        }

        private ByteArrayContent GetByteArrayContentForRequest<TModelToSend>(TModelToSend modelToSend)
        {
            var myContent = JsonConvert.SerializeObject(modelToSend);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return byteContent;
        }

        #endregion http client implementation
    }
}
