﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SevSharks.TogetherByTaxi.IntegrationTests.Models
{
    public class GetPreOrderByUniqIdResult
    {
        public Guid UniqIdentifier { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public string CreatedUserId { get; set; }
        public bool IsAuthorized { get; set; }
        public LocationResult LocationFrom { get; set; }
        public LocationResult LocationTo { get; set; }
        public bool IsOrderCreated { get; set; }
        public bool IsDeleted { get; set; }
    }
}
