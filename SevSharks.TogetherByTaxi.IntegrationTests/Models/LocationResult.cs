﻿namespace SevSharks.TogetherByTaxi.IntegrationTests.Models
{
    public class LocationResult
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Address { get; set; }
    }
}
