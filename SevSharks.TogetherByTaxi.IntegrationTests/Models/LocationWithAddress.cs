﻿namespace SevSharks.TogetherByTaxi.IntegrationTests.Models
{
    public class LocationWithAddress
    {
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
