﻿namespace SevSharks.TogetherByTaxi.IntegrationTests.Models
{
    public class PreOrderChangeModel
    {
        /// <summary>
        /// LocationFrom Lat
        /// </summary>
        public double LocationFromLat { get; set; }

        /// <summary>
        /// LocationFrom Lng
        /// </summary>
        public double LocationFromLng { get; set; }

        /// <summary>
        /// LocationFrom Address
        /// </summary>
        public string LocationFromAddress { get; set; }

        /// <summary>
        /// LocationTo Lat
        /// </summary>
        public double LocationToLat { get; set; }

        /// <summary>
        /// LocationTo Lng
        /// </summary>
        public double LocationToLng { get; set; }

        /// <summary>
        /// LocationTo Address
        /// </summary>
        public string LocationToAddress { get; set; }
    }
}
